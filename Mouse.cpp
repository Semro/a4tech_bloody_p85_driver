#include "Mouse.h"
#include <iostream>
#include <cstring>
#include <vector>
#include <cmath> // for pow()
#include <sstream>
#include <algorithm> // for remove()

using namespace std;

static int errorTimeout=3000;

void Mouse::init() {
	int ret = libusb_init(&context);
	if(ret < 0) {
		cerr<<"Init Error "<<ret<<endl;
		return;
	}
	libusb_set_option(context, LIBUSB_OPTION_LOG_LEVEL, 3);
	discoverDevices();
}

map<int, libusb_device_handle*> Mouse::getDevices() const {
	return devices;
}

void Mouse::discoverDevices() {
	libusb_device** devs;
	ssize_t cnt = libusb_get_device_list(context, &devs);
	if(cnt < 0) {
	cerr<<"Get Device Error"<<endl;
	return;
	}

	for(int i = 0; i < cnt; ++i) {
		libusb_device_descriptor desc;
		libusb_get_device_descriptor(devs[i], &desc);
		//libusb_get_endpoint_descriptor(devs[i],&desc);
		//libusb_get
		if (isCompatibleDevice(desc)) {
			if(libusb_open(devs[i], &currentDevice) != 0)
				continue;
			// interface_number needs to be 2 for normal mode, 0 for flashing mode
			if(libusb_kernel_driver_active(currentDevice, 2) == 1)
				if(libusb_detach_kernel_driver(currentDevice, 2) != 0) {
					libusb_close(currentDevice);
					continue;
				}
			devices.insert(pair<int, libusb_device_handle*>(libusb_get_device_address(devs[i]), currentDevice));


			// https://github.com/kristrev/portpilot-linux/blob/master/portpilot-logger-example.c#L88
			struct libusb_config_descriptor *conf_desc = NULL;
			const struct libusb_interface *intf;
			const struct libusb_interface_descriptor *intf_desc;
			const struct libusb_endpoint_descriptor *ep;

			libusb_get_active_config_descriptor(devs[i], &conf_desc);

			int xx, yy, zz;
			for (xx = 0; xx < conf_desc->bNumInterfaces; xx++) {
				intf = &conf_desc->interface[xx];

				for (yy = 0; yy < intf->num_altsetting; yy++) {
					intf_desc = &intf->altsetting[yy];
					for (zz = 0; zz < intf_desc->bNumEndpoints; zz++) {
						ep = &intf_desc->endpoint[zz];
						print_endpoint(ep);
					}
				}
			}
		}
	}

	libusb_free_device_list(devs, 1);
	if(devices.size() == 0){
		cout<<"No suitable device found."<<endl;
		return;
	}
	currentDevice = devices.begin()->second;
}

void Mouse::print_endpoint_comp(const struct libusb_ss_endpoint_companion_descriptor *ep_comp)
{
	printf("      USB 3.0 Endpoint Companion:\n");
	printf("        bMaxBurst:        %d\n", ep_comp->bMaxBurst);
	printf("        bmAttributes:     0x%02x\n", ep_comp->bmAttributes);
	printf("        wBytesPerInterval: %d\n", ep_comp->wBytesPerInterval);
}

void Mouse::print_endpoint(const struct libusb_endpoint_descriptor *endpoint)
{
	int i, ret;
	printf("      Endpoint:\n");
	printf("        bEndpointAddress: %02x\n", endpoint->bEndpointAddress);
	printf("        bmAttributes:     %02x\n", endpoint->bmAttributes);
	printf("        wMaxPacketSize:   %d\n", endpoint->wMaxPacketSize);
	printf("        bInterval:        %d\n", endpoint->bInterval);
	printf("        bRefresh:         %d\n", endpoint->bRefresh);
	printf("        bSynchAddress:    %d\n", endpoint->bSynchAddress);
	for (i = 0; i < endpoint->extra_length;) {
		if (LIBUSB_DT_SS_ENDPOINT_COMPANION == endpoint->extra[i + 1]) {
			struct libusb_ss_endpoint_companion_descriptor *ep_comp;

			ret = libusb_get_ss_endpoint_companion_descriptor(NULL, endpoint, &ep_comp);
			if (LIBUSB_SUCCESS != ret) {
				continue;
			}

			print_endpoint_comp(ep_comp);

			libusb_free_ss_endpoint_companion_descriptor(ep_comp);
		}

		i += endpoint->extra[i];
	}
}

bool Mouse::isCompatibleDevice(libusb_device_descriptor &desc) {
	if(desc.idVendor != A4TECH_VID)
		return false;
//	for(size_t i = 0; i < COMPATIBLE_PIDS_SIZE; ++i)
//		if(desc.idProduct == COMPATIBLE_PIDS[i])
	return true;
//	return false;
}

Mouse::~Mouse() {
	for (auto& dev : devices) {
		libusb_close(dev.second);
	}
	if(context != nullptr) {
		libusb_exit(context);
	}
}




void Mouse::setLEDs(size_t bytesNeeded, uint8_t LEDS[3][8]) {
	//07 030602 00 00 00 00 ff0000 000000 ff00ff 7fff00 7f00ff 00ff00 00ffff 007fff 0000000000000000000000000000000000000000000000000000000000000000
	vector<uint8_t> request = {A4TECH_MAGIC, 0x03,0x06,0x02,0x00,0x00,0x00,0x00,
	LEDS[0][0],LEDS[1][0],LEDS[2][0],
	LEDS[0][1],LEDS[1][1],LEDS[2][1],
	LEDS[0][2],LEDS[1][2],LEDS[2][2],
	LEDS[0][3],LEDS[1][3],LEDS[2][3],
	LEDS[0][4],LEDS[1][4],LEDS[2][4],
	LEDS[0][5],LEDS[1][5],LEDS[2][5],
	LEDS[0][6],LEDS[1][6],LEDS[2][6],
	LEDS[0][7],LEDS[1][7],LEDS[2][7]};
	request.resize(bytesNeeded);
//	if(level < 0 || level > 3) {
//		return -1;
//	}
//	if(
	writeToMouse(request, request.size()); //) < 0) {
//		return -2;
//	}
//	return 0;
}


// From when we were dealing with dynamic arrays and not vectors
/*int Mouse::writeToMouse(uint8_t data[], size_t size) {
// wIndex needs to be 2 for normal mode, 0 for flashing mode
	std::cout << "size in writeToMouse: " << size << endl;
	int res = libusb_control_transfer(currentDevice,0x21,9,0x0307,2,data,size,errorTimeout);
	if(res < 0) {
		cout<<"Unable to send command"<<endl;
		return -1;
	}
	return 0;
}*/

int Mouse::writeToMouse(vector<uint8_t> data, size_t size) {
	// wIndex needs to be 2 for normal mode, 0 for flashing mode
	int res = libusb_control_transfer(currentDevice,0x21,9,0x0307,2,data.data(),size,errorTimeout);
	if(res < 0) {
		cout<<"Unable to send command"<<endl;
		return -1;
	}
	return 0;
}

// From when we were dealing with dynamic arrays and not vectors
/*
//readFromMouse(request, sizeof(request), response, sizeof(response));
int Mouse::readFromMouse(uint8_t *request, size_t requestSize, uint8_t *response, size_t responseSize) {
	if(writeToMouse(request, requestSize) < 0){
		return -1;
	}
// wIndex needs to be 2 for normal mode, 0 for flashing mode
	int res = libusb_control_transfer(currentDevice,0xa1,1,0x0307,2,response,responseSize,errorTimeout);
	if(res < 0){
		cout<<"Unable to receive data"<<endl;
		return -2;
	}
	return 0;
}*/

int Mouse::readFromMouse(vector<uint8_t> request, size_t requestSize, vector<uint8_t> * response, size_t responseSize) {
	if(writeToMouse(request, requestSize) < 0){
		return -1;
	}
// wIndex needs to be 2 for normal mode, 0 for flashing mode
	int res = libusb_control_transfer(currentDevice,0xa1,1,0x0307,2,response->data(),responseSize,errorTimeout);
	if(res < 0){
		cout<<"Unable to receive data"<<endl;
		return -2;
	}
	return 0;
}

uint8_t Mouse::getLEDBrightness(size_t bytesNeeded) {
	vector<uint8_t> request = {A4TECH_MAGIC, BACKLIGHT_OPCODE,0x00,0x00,BACKLIGHT_READ,0x00};
	request.resize(bytesNeeded);
	vector<uint8_t> response(bytesNeeded);

	if (readFromMouse(request, request.size(), &response, response.size()) < 0 ) { // If we got an error
		return 255;
	}
	return response[8];
}

int Mouse::setLEDBrightness(size_t bytesNeeded, uint8_t level) {
	vector<uint8_t> request = {A4TECH_MAGIC, BACKLIGHT_OPCODE,0x00,0x00,BACKLIGHT_WRITE,0x00,0x00,0x00, level, 0x00};
	request.resize(bytesNeeded);
	if(level > 3) {
		return -1;
	}
	if(writeToMouse(request, request.size()) < 0) {
		return -2;
	}
	return level;
}

void Mouse::restartFirmware(size_t bytesNeeded) {// THIS CAN FUCK CHINESE USB HUBS UP - mine temporarily turned off after a few restarts
	vector<uint8_t> request = {A4TECH_MAGIC, 0x00};
	request.resize(bytesNeeded);
	writeToMouse(request, request.size());
	//readFromMouse(request, request.size(), &response, response.size());
}

void Mouse::RGBEditMode(size_t bytesNeeded, string toggle) {
	if (toggle == "enable") {
		vector<uint8_t> request = {A4TECH_MAGIC, 0x03, 0x06, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01};
		request.resize(bytesNeeded);
		writeToMouse(request, request.size());
	}
	if (toggle == "disable") {
		vector<uint8_t> request = {A4TECH_MAGIC, 0x03, 0x06, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
		request.resize(bytesNeeded);
		writeToMouse(request, request.size());
	}
	if (toggle != "enable" && toggle != "disable") {
		cout << "WRONG USE OF RGBEDITMODE" << endl;
		return;
	}
	//readFromMouse(request, request.size(), &response, response.size());
}

void Mouse::SetSensitivity(size_t bytesNeeded, uint8_t modesEnabled, uint8_t modeSelected, int currentDPI, uint8_t responseRate) {
	// It's actually possible to use 0x00 for the final value and it is slower than 0x01, but Bloody 6 won't let you do it, so we won't either
	if (currentDPI <= 0 || currentDPI > 50) {
		cout << "WRONG USE OF SetSensitivity" << endl;
		return;
	}
	//07 0d 0000 0000 0000 04 00 0101 00 01 0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000 # 100Hz
	// modesEnabled; // 00 to 04
	// modeSelected; // 00 to 04 - sets what mode is going to be next when "next DPI mode" button on mouse is pressed
	// responseRate; // Response rate. 00 for 3Hz, 01 for 1000hz, 02 for 500Hz, 04 for 250Hz, 08 for 125Hz. Can be set to values like 03 for 666Hz[average] (1x 1000Hz, 1x 333Hz), which is something the v6 doesn't do (cause why would it)

	int input = currentDPI;
	int x = 0;
	for (int i = 0; i < input; i++){
		if (i % 3 == 1){
			x++;
		}
	}
	int y = input-x-1;
	//cout << "x:"<< x <<" y:"<< y <<" i:"<< input << endl;
	uint8_t DPIByte = 0x01 + 0x03*x + 0x02*y;

	vector<uint8_t> request = {A4TECH_MAGIC, 0x0d, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, modesEnabled, modeSelected, DPIByte, DPIByte, 0x00, responseRate};
	request.resize(bytesNeeded);
	writeToMouse(request, request.size());

	//readFromMouse(request, request.size(), &response, response.size());
}

vector<uint8_t> Mouse::getRNG(size_t bytesNeeded) {
	vector<uint8_t> request = {A4TECH_MAGIC, 0x10};
	request.resize(bytesNeeded);
	vector<uint8_t> response(bytesNeeded);
	readFromMouse(request, request.size(), &response, response.size());
	return vector<uint8_t> {response[8], response[9]};
}

string Mouse::getMouseID(size_t bytesNeeded) {
	vector<uint8_t> request = {A4TECH_MAGIC, 0x05};
	request.resize(bytesNeeded);
	vector<uint8_t> response(bytesNeeded);
	readFromMouse(request, request.size(), &response, response.size());

	std::stringstream stream;
	stream << hex << static_cast<int>(response[31]) << static_cast<int>(response[30]) << static_cast<int>(response[29]) << static_cast<int>(response[28]);

	return stream.str();// printf("%02x", response[30]); printf("%02x", response[29]); printf("%02x", response[28]);
}
vector<uint8_t> Mouse::setMouseID(size_t bytesNeeded) {
	// This function does not really work.
	vector<uint8_t> request = {A4TECH_MAGIC, 0x01,0x18,0x3f,0xf4,0x60,0x57,0xd8,0xae,0x4c,0xe5,0xc3,0x54,0x2e,0x46,0x51};
	request.resize(bytesNeeded);
	vector<uint8_t> response(bytesNeeded);
	// 0701 183f f460 57d8 ae4c e5c3 542e 4651 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000
	readFromMouse(request, request.size(), &response, response.size());
	return response;
}

uint8_t Mouse::getSensorCalibration(size_t bytesNeeded) {
	vector<uint8_t> request { A4TECH_MAGIC, 0x13 };
	request.resize(bytesNeeded);
	vector<uint8_t> response(bytesNeeded);
	readFromMouse(request, request.size(), &response, response.size());
	return response[8];
}

void Mouse::setSensorCalibration(size_t bytesNeeded, int presetEnabled,int calibration) {
	uint8_t leftBits;
	uint8_t rightBits;
	switch(calibration){
		case 1: rightBits=0x0f; break;
		case 2: rightBits=0x0e; break;
		case 3: rightBits=0x0d; break;
		case 4: rightBits=0x0c; break;
		case 5: rightBits=0x0b; break;
		case 6: rightBits=0x0a; break;
		case 7: rightBits=0x09; break;
		case 8: rightBits=0x08; break;
		case 9: rightBits=0x07; break;
		case 10: rightBits=0x06; break;
		case 11: rightBits=0x05; break;
		case 12: rightBits=0x04; break;
		case 13: rightBits=0x03; break;
		case 14: rightBits=0x02; break;
		case 15: rightBits=0x01; break;
		case 16: rightBits=0x00; break;
	}
	switch(presetEnabled){
		case 0: leftBits = 0x0c; break;
		case 1: leftBits = 0x08;break;
	}
	uint8_t calibrationByte = assembleToUint8t(leftBits, rightBits);
	vector<uint8_t> request = {A4TECH_MAGIC, 0x013,0x00,0x00,0x80,0x00,0x00,0x00,calibrationByte};
	request.resize(bytesNeeded);
	vector<uint8_t> response(bytesNeeded);
	// 0701 183f f460 57d8 ae4c e5c3 542e 4651 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000
	readFromMouse(request, request.size(), &response, response.size());
}

uint8_t Mouse::assembleToUint8t (uint8_t left, uint8_t right){
	return (((left & 0xF) << 4) | ( right & 0xF));
}
uint8_t Mouse::leftUint8t (uint8_t fuck) {
	return (fuck >> 4) & 0xF;
}
uint8_t Mouse::rightUint8t (uint8_t fuck) {
	return fuck & 0xF;
/*
  foo = (byte >> 4) & 0xF;  //get high
  foo = byte & 0xF; // get low
  foo = (((high & 0xF) << 4) | ( low & 0xF); // assemble
*/
}

void Mouse::dumpMemory(size_t bytesNeeded) {
	// Not sure if this dumps everything, mouse claims to have 160KB which seems to be confirmed by the size of the firmare, this dumps 65536 bytes however. (it repeats after a while)
	cout << "Memory dump:" << endl;
	uint8_t firstAddress = 0x00;
	uint8_t secondAddress = 0x00;
	int x = 0;
	for (int y = 0; y < 256; y++){
		// This is 0x00 0x00 we're bruteforcing
		while(x<256){
			vector<uint8_t> request = {A4TECH_MAGIC, 0x02,0x00,firstAddress,secondAddress,0x60}; // 0x60 can seemingly be anything
			request.resize(bytesNeeded);
			vector<uint8_t> response(bytesNeeded);
			readFromMouse(request, request.size(), &response, response.size());
			printf("%02x", firstAddress); printf("%02x", secondAddress); cout << ": ";
			//cut off 070200000060 0000
			// needs to be ran through cut -c-118. Maybe don't do it for each bytesNeeded but for 64? Otherwise it gets maimed by 72 and needs a cut
			for (int i{8}; i < bytesNeeded; i++) {
				printf("%02x", response[i]);
			}
			cout << endl;
			// There's 112 chars per line, and 1 position jumps 4 chars, therefore 28 (*4=112)
			secondAddress+=28;
			x+=28;
		}
		// We're doing 0x00 0x00 + 0x01 0x00, which means the second element overflowed, but there might be remainder, so instead of resetting `x` we simply substract 256
		x-=256;
		firstAddress++;
	}
}

string Mouse::getInitStatus(size_t bytesNeeded) {
	vector<uint8_t> request = {A4TECH_MAGIC, 0x02, 0x00, 0x01, 0x04, 0x60};
	request.resize(bytesNeeded);
	vector<uint8_t> response(bytesNeeded);
	readFromMouse(request, request.size(), &response, response.size());
	uint8_t notInit = 0x6d;
	if(notInit == response[14]) {
		cout << "THIS MOUSE IS NOT INITIALIZED - THIS DRIVER CANNOT INITIALIZE THE MOUSE, IT IS SUGGESTED YOU DO NOT USE THIS TOOL BEFORE DOING INITIALIZATION IN THE BLOODY V6 DRIVER." << endl;
		return "NO";
	}
	return "YES";
}

// This function is a "simple" additive function - it adds: mult, secMult, secMult, mult,... and so on.
// You can also add a base value. This function can be probably removed after some parts are properly reworked into working with bitmasks.
uint8_t Mouse::bitmaskAlgo(int input, int multiplier, int secondaryMultiplier, int baseValue){
	int x = 0;
	for (int i = 0; i < input; i++){
		if (i % 3 == 1){
			x++;
		}
	}
	int y = input-x-1;
	uint8_t DPIByte = multiplier*x + secondaryMultiplier*y + baseValue;
//	cout << "FinalValue: "<< hex << unsigned(DPIByte) << endl;
	return DPIByte;
}

vector<uint8_t> Mouse::getMagicSensBytes(int firstSens, int modesEnabled, int selectedMode, int sensorCalibration, int calibrationPreset, int responseRate){
	//int firstSens, int modesEnabled, int selectedMode, int sensorCalibration, int calibrationPreset, int responseRate

	// Sensitivity        - firstSens         - 1 to 50; 1 is default (100 to 5000Hz)
	// Number of modes    - modesEnabled      - 1 to 5;  1 is default
	// Selected mode      - selectedMode      - 1 to 5;  1 is default
	// Sensor calibration - sensorCalibration - 1 to 16; 1 is default (16 should be but am a lazy coder)
	//                      calibrationPreset - 0 or 1;  1 is default
	// Response rate      - responseRate      - 1 to 4;  1 is default (1=1000Hz, 2=500, 3=250, 4=125)

	// rewrite into proper bitmasks #TODO
/*	int firstByteINT = int(0x2b); // Old FW only?
	int secondByteINT = int(0x37);
	int thirdByteINT = int(0x1a);
	int fourthByteINT = int(0x55);
*/
	// Default bitmask for P93 2008.1005 (P series as a whole most likely) for [100Hz], RR 1000Hz, calibration 1;preset enabled.
	int firstByteINT = int(0x25);
	int secondByteINT = int(0x33);
	int thirdByteINT = int(0xe1);
	int fourthByteINT = int(0x2e);
	// Sensitivity
	// It's the first sens, other 4 DPI sens are set elsewhere
	firstByteINT+=bitmaskAlgo(firstSens, 6, 4, 0);
	secondByteINT+=firstSens*4-4;
	thirdByteINT+=bitmaskAlgo(firstSens, 39, 26, 0);
	fourthByteINT+=(firstSens*26-26)+(firstSens-1)/9;
	if (firstSens == 50){ // Different value when 50, why?
		firstByteINT-=2;
		thirdByteINT-=13;
	}

	// Number of modes
	firstByteINT+=modesEnabled-1;
	thirdByteINT+=modesEnabled*4-4;

	// Selected mode
	firstByteINT+=selectedMode-1;
	thirdByteINT+=selectedMode*5-5;

	// Sensor Calibration
	// +01 00 +1f 00
	if (calibrationPreset == 0){
		firstByteINT+=64;
		thirdByteINT+=192;
		fourthByteINT+=7;
	}
	firstByteINT-=sensorCalibration-1;
	thirdByteINT-=sensorCalibration*31-31;

	// Response rate
	firstByteINT+=pow(2,responseRate-2); //abusing that 0.5 = 0 in int;         1=0, 2=1,  3=2,  4=4
	thirdByteINT+=int(pow(2,responseRate-2))*30; //abusing that 0.5 = 0 in int; 1=0, 2=30, 3=60, 4=120

	// Didn't find how second byte over/underflows. Fourth doesn't seem to under/overflow.
	// And this approach is hacky as fuck in the first place.
	while (firstByteINT > 256){
		firstByteINT-=256;
		secondByteINT+=1;
	}
	while (firstByteINT < 0){
		firstByteINT+=256;
		secondByteINT-=1;
	}

	while (thirdByteINT > 256){
		thirdByteINT-=256;
		fourthByteINT+=1;
	}
	while (thirdByteINT < 0){
		thirdByteINT+=256;
		fourthByteINT-=1;
	}
	//Debug info
/*
	cout << hex;
	cout << << unsigned(firstByteINT) << " " << unsigned(secondByteINT)
		<< " " << unsigned(thirdByteINT) << " " << unsigned(fourthByteINT) << endl;
	cout << dec;
	cout << "First:" << to_string(firstByteINT) << " Second:" << to_string(secondByteINT) << " Third:" << to_string(thirdByteINT)
		<< " Fourth:" << to_string(fourthByteINT) <<endl;
*/
	vector<uint8_t> magicBytes;
	magicBytes.push_back(firstByteINT);
	magicBytes.push_back(secondByteINT);
	magicBytes.push_back(thirdByteINT);
	magicBytes.push_back(fourthByteINT);
	return magicBytes;
}

void Mouse::saveSensToMemory(size_t bytesNeeded, uint8_t modesEnabled, uint8_t modeSelected, int currentDPI, uint8_t responseRate, int presetEnabled,int calibration, int DPIMode1, int DPIMode2, int DPIMode3, int DPIMode4, int DPIMode5){
	vector<uint8_t> request;
	vector<uint8_t> response(bytesNeeded);
	uint8_t calibrationByte;
	if(presetEnabled == 1){
		calibrationByte = 0x10 - calibration;
	}
	else{
		calibrationByte = assembleToUint8t(4, 0x10 - calibration);
	}
	vector<uint8_t> magicBytes = getMagicSensBytes(currentDPI, modesEnabled, modeSelected, calibration, presetEnabled, responseRate);
	vector<uint8_t> DPIarray = {
		bitmaskAlgo(DPIMode1, 3, 2, 1), bitmaskAlgo(DPIMode1, 2, 2 ,2),
		bitmaskAlgo(DPIMode2, 3, 2, 1), bitmaskAlgo(DPIMode2, 2, 2 ,2),
		bitmaskAlgo(DPIMode3, 3, 2, 1), bitmaskAlgo(DPIMode3, 2, 2 ,2),
		bitmaskAlgo(DPIMode4, 3, 2, 1), bitmaskAlgo(DPIMode4, 2, 2 ,2),
		bitmaskAlgo(DPIMode5, 3, 2, 1), bitmaskAlgo(DPIMode5, 2, 2 ,2),
	};

	// If magic bytes in memory match the result, do nothing.
	//# TODO
	//Q0702 002a 0060 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000
	//R0702 002a 0060 0000 a4a4 1d37 6753 0000 0000 0000 0102 0102 0000 2e28 2e28 0000 453c 453c 0000 5c50 5c50 0000 7364 7364 0000 0000 0000 0000 0000 0000 0000 0000
	request = {0x07,0x02,0x00,0x2a,0x00,0x60};
	request.resize(bytesNeeded);
	readFromMouse(request, request.size(), &response, response.size());

	// Save to memory
	//Q0701 782a 00e0 0000 ffff 1c37 4953 0000 0000 0000 0102 0102 0000 2e28 2e28 0000 453c 453c 0000 5c50 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000
	//R0701 782a 00e0 0000 ffff 1c37 4953 0000 0000 0000 0102 0102 0000 2e28 2e28 0000 453c 453c 0000 5c50 ffff ffff ffff ffff ffff ffff ffff ffff ffff ffff ffff ffff
	request = {0x07,0x01, 0x78,0x2a, 0x00,0xe0, 0x00,0x00 ,0xff,0xff ,magicBytes[0],magicBytes[1] ,magicBytes[2],magicBytes[3], 0x00,0x00, modesEnabled,0x00, modeSelected,0x00
			   ,DPIarray[0],DPIarray[1], DPIarray[0],DPIarray[1], 0x00,0x00, DPIarray[2],DPIarray[3], DPIarray[2],DPIarray[3], 0x00,0x00, DPIarray[4],DPIarray[5], DPIarray[4],DPIarray[5], 0x00,0x00, DPIarray[6],DPIarray[7]};
	request.resize(bytesNeeded);
	readFromMouse(request, request.size(), &response, response.size());
	//Q0701 782a 1060 0000 5c50 0000 7364 7364 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0100 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000
	//R0701 782a 1060 0000 5c50 0000 7364 7364 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0100 0000 ffff ffff ffff ffff ffff ffff ffff ffff ffff ffff ffff ffff
	request = {0x07,0x01, 0x78,0x2a, 0x10,0x60, 0x00,0x00, DPIarray[6],DPIarray[7], 0x00,0x00, DPIarray[8],DPIarray[9], DPIarray[8],DPIarray[9], 0x00,0x00, 0x00,0x00, 0x00,0x00, 0x00,0x00, 0x00,0x00, 0x00,0x00, 0x00,0x00, 0x00,0x00, 0x00,0x00, 0x00,0x00, responseRate,0x00, calibrationByte };
	request.resize(bytesNeeded);
	readFromMouse(request, request.size(), &response, response.size());
	//Q0701 782a 2060 0000 0000 0000 0000 0000 0700 0000 0100 0300 0400 0500 0600 0700 0800 0900 0b00 0c00 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000
	//R0701 782a 2060 0000 0000 0000 0000 0000 0700 0000 0100 0300 0400 0500 0600 0700 0800 0900 0b00 0c00 ffff ffff ffff ffff ffff ffff ffff ffff ffff ffff ffff ffff
	request = {0x07,0x01, 0x78,0x2a, 0x20,0x60, 0x00,0x00, 0x00,0x00, 0x00,0x00, 0x00,0x00, 0x00,0x00, 0x07,0x00, 0x00,0x00, 0x01,0x00, 0x03,0x00, 0x04,0x00, 0x05,0x00, 0x06,0x00, 0x07,0x00, 0x08,0x00, 0x09,0x00, 0x0b,0x00, 0x0c,0x00,};
	request.resize(bytesNeeded);
	readFromMouse(request, request.size(), &response, response.size());
	//Q0701 782a 3060 0000 0d00 0e00 0f00 1000 1200 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000
	//R0701 782a 3060 0000 0d00 0e00 0f00 1000 1200 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 ffff ffff ffff ffff ffff ffff ffff ffff ffff ffff ffff ffff
	request = {0x07,0x01, 0x78,0x2a, 0x30,0x60, 0x00,0x00, 0x0d,0x00, 0x0e,0x00, 0x0f,0x00, 0x10,0x00, 0x12,0x00};
	request.resize(bytesNeeded);
	readFromMouse(request, request.size(), &response, response.size());
	//Q0701 782a 4060 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000
	//R0701 782a 4060 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 ffff ffff ffff ffff ffff ffff ffff ffff ffff ffff ffff ffff
	request = {0x07,0x01,0x78,0x2a,0x40,0x60};
	request.resize(bytesNeeded);
	readFromMouse(request, request.size(), &response, response.size());
	//Q0701 782a 5060 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000
	//R0701 782a 5060 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 ffff ffff ffff ffff ffff ffff ffff ffff ffff ffff ffff ffff
	request = {0x07,0x01,0x78,0x2a,0x50,0x60};
	request.resize(bytesNeeded);
	readFromMouse(request, request.size(), &response, response.size());
	//Q0701 782a 6060 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000
	//R0701 782a 6060 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 ffff ffff ffff ffff ffff ffff ffff ffff ffff ffff ffff ffff
	request = {0x07,0x01,0x78,0x2a,0x60,0x60};
	request.resize(bytesNeeded);
	readFromMouse(request, request.size(), &response, response.size());
	//Q0701 782a 7060 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000
	//R0701 782a 7060 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 ffff ffff ffff ffff ffff ffff ffff ffff ffff ffff ffff ffff
	request = {0x07,0x01,0x78,0x2a,0x70,0x60};
	request.resize(bytesNeeded);
	readFromMouse(request, request.size(), &response, response.size());
	//Q0701 002a 0060 0000 a4a4 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000
	request = {0x07,0x01, 0x00,0x2a, 0x00,0x60, 0x00,0x00, 0xa4,0xa4};
	request.resize(bytesNeeded);
	writeToMouse(request, request.size());
//	readFromMouse(request, request.size(), &response, response.size());

	// Set instant settings and check things were written properly

	//Q0713 0000 8000 0000 8000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000
	//Q070d 0000 0000 0000 0000 0101 0001 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000
	//Q0702 002a 0060 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000
	//R0702 002a 0060 0000 a4a4 1c37 4953 0000 0000 0000 0102 0102 0000 2e28 2e28 0000 453c 453c 0000 5c50 5c50 0000 7364 7364 0000 0000 0000 0000 0000 0000 0000 0000

}
vector<uint8_t> Mouse::testCode(size_t bytesNeeded, std::string code) {
	// Strip spaces
	std::string::iterator end_pos = std::remove(code.begin(), code.end(), ' ');
	code.erase(end_pos, code.end());
	if (code.length() != bytesNeeded *2) {
		cout << "Length wrong: " << code.length() << " expected: " << bytesNeeded*2 << " for: " << code << endl;
		return vector<uint8_t>();
	}

	vector<uint8_t> request(bytesNeeded);

	for(int i = 0; i < bytesNeeded; i++) {
		request[i] = stoi(code.substr(2*i, 2), 0, 16);
	}

	cout << "Sending command..." << endl;
	vector<uint8_t> response(bytesNeeded);
	readFromMouse(request, request.size(), &response, response.size());
	cout << "Response:" << endl;
	for (int j = 0; j < bytesNeeded; ++j) {
		printf("%02x", static_cast<int>(response[j]));
	}
	cout << endl;
	return response;
}

string Mouse::getDeviceNameById(const int & id) const {
	string name = "Unknown";
	switch (id){
		case BLOODY_V5_PID:
			name = "Bloody V5";
			break;
		case BLOODY_V7_PID:
			name = "Bloody V7";
			break;
		case BLOODY_V8_PID:
			name = "Bloody V8";
			break;
		case BLOODY_R7_PID:
			name = "Bloody R7";
			break;
		case BLOODY_R3_PID:
			name = "Bloody R3";
			break;
		case BLOODY_AL9_PID:
			name = "Bloody AL9";
			break;
		case BLOODY_R70_PID:
			name = "Bloody R70";
			break;
		case BLOODY_A9_PID:
			name = "Bloody A9";
			break;
		case BLOODY_P85_PID:
			name = "Bloody P85";
			break;
		case BLOODY_P85_C3_PID:
			name = "Bloody P85_C3";
			break;
		case BLOODY_P93_PID:
			name = "Bloody P93";
			break;
	}
	return name;
}

std::string Mouse::uint8_vector_to_hex_string(const vector<uint8_t>& v) {
	std::string result;
	result.reserve(v.size() * 2);   // two digits per character
	static constexpr char hex[] = "0123456789ABCDEF";
	for (uint8_t c : v) {
		result.push_back(hex[c / 16]);
		result.push_back(hex[c % 16]);
	}
	return result;
}

std::string Mouse::AddSeparators(const std::string & s) {
	if(s.size() <= 1) {
		return s;
	}
	std::string r;
	r.reserve((s.size()*2)-1);
	r.push_back(s[0]);
	for(size_t i = 1; i < s.size(); ++i) {
		if (i % 4 == 0 ) {
			r.push_back(' ');
		}
		r.push_back(s[i]);
	}
	return r;
}

vector< vector<string> > Mouse::listDevices() {
	vector< vector<string> > processedDevices;
	size_t i = 0;
	for(auto &devHand : devices) {
		libusb_device* device = libusb_get_device(devHand.second);
		libusb_device_descriptor desc;
		libusb_get_device_descriptor(device, &desc);
		string name = getDeviceNameById(desc.idProduct);
		cout<< to_string(devHand.first)<<":"<<name<<" ["<< hex << desc.idProduct<<"]"<<endl << dec;
		printf("      Device info:\n");
		printf("        bDescriptorType: %02x\n", desc.bDescriptorType);
		printf("        bDeviceClass:     %02x\n", desc.bDeviceClass);
		printf("        iManufacturer:   %d\n", desc.iManufacturer);
		printf("        iProduct:        %d\n", desc.iProduct);
		printf("        bcdDevice:        %02x\n", desc.bcdDevice);
		printf("        iSerialNumber:         %d\n", desc.iSerialNumber);
		printf("        idProduct:    %02x\n", desc.idProduct);
		printf("        idVendor:    %02x\n", desc.idVendor);
		processedDevices.push_back( std::vector<string>() );
		processedDevices[i].push_back(to_string(devHand.first));
		processedDevices[i].push_back(name);
		processedDevices[i].push_back(to_string(desc.idProduct));
		processedDevices[i].push_back(to_string(desc.bcdDevice));
		i++;
	}
	return processedDevices;
}

bool Mouse::selectDevice(int address) {
	if(devices.count(address) == 0)
		return false;
	currentDevice = devices.at(address);
	return true;
}

std::string Mouse::string_to_hex(const std::string& input)
{
	std::stringstream stream;
	stream << hex << stoi(input);
	return stream.str();
}

std::string Mouse::hex_to_string(std::string& input)
{
	static const char* const lut = "0123456789ABCDEF";
	size_t len = input.length();
	if (len & 1) {
		input = "0" + input;
	}

	std::string output;
	output.reserve(len / 2);
	for (size_t i = 0; i < len; i += 2)
	{
		char a = input[i];
		const char* p = std::lower_bound(lut, lut + 16, a);
		if (*p != a) throw std::invalid_argument("not a hex digit");

		char b = input[i + 1];
		const char* q = std::lower_bound(lut, lut + 16, b);
		if (*q != b) throw std::invalid_argument("not a hex digit");

		output.push_back(((p - lut) << 4) | (q - lut));
	}
	return output;
}
