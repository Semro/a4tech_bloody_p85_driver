// Created by maxmati on 4/18/15.
// Forked by C0rn3j sometime in 2017.
#include "stdafx.h" // Cause Windows
#include <string> // Cause Windows
#include <iostream>
#include "Mouse.h"
#include <vector>
//#include <unistd.h> //for usleep
using namespace std;

int main() {
	cout << "This CLI tool is more for testing if anything, ideally use the GUI version!" << endl;
	Mouse m;
// testing
	vector<uint8_t> vectresponse = m.getMagicSensBytes(1,1,1,1,1,1);
	cout << hex << unsigned(vectresponse[0]) << " "<< unsigned(vectresponse[1]) << " "<< unsigned(vectresponse[2]) << " "<< unsigned(vectresponse[3]) << endl << dec;
// end of testing
	int address;
	m.init();
	do {
		const auto devices = m.getDevices();
		if(devices.size() == 1) {
			libusb_device* device = libusb_get_device(devices.begin()->second);
			libusb_device_descriptor desc;
			libusb_get_device_descriptor(device, &desc);
			cout << "Only one device found, selecting it: " << m.getDeviceNameById(desc.idProduct) <<" ["<< hex << desc.idProduct<<"]"<< endl;
			m.selectDevice(desc.idProduct);
			break;
		} else {
				do{
					m.listDevices();
					cout << "Enter device address: ";
					cin >> address;
					if (cin.fail()) {
						cerr<<"Error, bad input"<<endl;
						cin.clear();
						cin.ignore(512,'\n');
						continue;
					}
					cout << "Selected addresss: " <<address<<endl;
					break;
				}while(true);
		}
	} while(!m.selectDevice(address));

	int protocolBytes;
	// Protocol test - very shoddy detection
	cout << "DETECTING MOUSE PROTOCOL (Takes long because it's shoddy)" << endl;

	if ((int)m.getLEDBrightness(72) == 255) {
		if ((int)m.getLEDBrightness(64) != 255) {
			protocolBytes = 64;
			cout<<"Detected device as one that uses 64 bytes in the protocol."<< endl;
		}
		else {
				cerr<<"UNSUPPORTED DEVICE"<<endl;
				return 1;
		}
		// We got an error trying to use 72 bytes, and as there are only 2 known protocol, the mouse must therefore use 64 bytes.
	}
	else {
		protocolBytes = 72;
		cout<<"Detected device as one that uses 72 bytes in the protocol."<< endl;
	}
	cout<<"Current LED brightness: "<<(int)m.getLEDBrightness(protocolBytes)<< endl;

	//Check if the mouse is initialized
	string initQuery = m.getInitStatus(protocolBytes);
	if(initQuery == "YES"){
		cout<<"Mouse initialized! You can use it with this tool"<<endl; // This doesn't work on some models, but the ID on them seems to still be ffffff, so let's detect that too?
	}
	if(initQuery == "NO"){
		cerr<<"Mouse not initialized! You shouldn't use it with this tool for anything else than memory dump before first connecting it to the official bloody software."<<endl;
	}


	m.getMouseID(protocolBytes);
//	cout << "Mouse ID is: "; printf("%02x", baseQuery[31]); printf("%02x", baseQuery[30]); printf("%02x", baseQuery[29]); printf("%02x", baseQuery[28]); cout << endl;

	do{ //Main cycle
		int choice;
		cout << "Pick what to do:" <<endl;
		cout << "1) Enter RGB edit mode (only tested on P series, should only work for J and P series)" <<endl;
		cout << "2) Set backlight intensity" <<endl;
		cout << "3) Sensitivity settings (only tested on P series)" <<endl;
		cout << "4) Set DPI" <<endl;
		cout << "5) Test codes" <<endl;
		cout << "6) Restart mouse firmware and exit (for debug purposes)" <<endl;
		cout << "7) Dump memory (for debug purposes)" <<endl;

		cin >> choice;
		if (choice < 9 && choice > 0 && !cin.fail()) {
			switch(choice) {
				case 1:
					m.RGBEditMode(protocolBytes, "enable");
					uint8_t firstLEDarray[3][8];// = {levelz, levelz, levelz};
					do {
/*						cout << "Enter any number but 0 to start setting LED colors. 0 to exit."<<endl;
						int doWeContinue;
						cin >> doWeContinue;
						if (doWeContinue == 0) break;
*/
						cout<<"Enter LED position from 0 to 7 to set and three values from 0 to 255 for RGB (example - 0 123 120 255): "<<endl;
						int colorValueRed, colorValueGreen, colorValueBlue;
						int LEDPosition = 0;
						cin >> LEDPosition >> colorValueRed >> colorValueGreen >> colorValueBlue;
						if (cin.fail() || LEDPosition > 7 || LEDPosition < 0 || colorValueRed < 0 || colorValueRed > 255 || colorValueGreen < 0 || colorValueGreen > 255 || colorValueBlue < 0 || colorValueBlue > 255) {
							cerr<<"Error, bad input"<<endl;
							cin.clear();
							cin.ignore(512,'\n');
							break;
						}
						firstLEDarray[0][LEDPosition] = (uint8_t) strtoul(to_string(colorValueRed).c_str(), nullptr, 10);
						firstLEDarray[1][LEDPosition] = (uint8_t) strtoul(to_string(colorValueGreen).c_str(), nullptr, 10);
						firstLEDarray[2][LEDPosition] = (uint8_t) strtoul(to_string(colorValueBlue).c_str(), nullptr, 10);
						m.setLEDs(protocolBytes, firstLEDarray);
					} while(true);
					m.RGBEditMode(protocolBytes, "disable");
					break;
				case 2:
					int level;
					cout<<"Current backlight level: "<<(int)m.getLEDBrightness(protocolBytes)<<endl;
					do {
						cout << "Select backlight level(0-3):";
						cin >> level;
						if (cin.fail()){
							cerr<<"Error, bad input"<<endl;
							cin.clear();
							cin.ignore(512,'\n');
							continue;
						}
					} while(m.setLEDBrightness(protocolBytes, level) < 0); break;
				case 3:
					cout << "3" << endl; break;
				case 4:
					cout << "Enter a value from 1 to 50 to set DPI (1 for 100Hz, 2 for 200Hz,...)" << endl;
					int DPIValue;
					cin >> DPIValue;
					m.SetSensitivity(protocolBytes, 0x00, 0x00, DPIValue, 0x01);
					break;
				case 5:
					do {
						cout << "WRITE YOUR COMMAND" << endl;
						std::string code;
						std::getline(cin, code);
						m.testCode(protocolBytes, code);
					} while(true); break;
				case 6:
					m.restartFirmware(protocolBytes); return 0;
				case 7:
					m.dumpMemory(protocolBytes); break;
				default: continue;
			}
		}
		else { // When input fails
			cerr << "Error, bad input" << endl;
			cin.clear();
			cin.ignore(512,'\n');
		}
	}while(true);
	//	uint8_t msg[64] = { 07, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00 };
	//	m.testCode(msg);
}
