#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFile>
#include <QTextStream>
#include <QMessageBox>
#include <iostream>
#include "../Mouse.h"
#include <vector>
#include <sstream>
#include <unistd.h> //for usleep
#include <stdio.h>

/*
 * Current problems and TODOs:
 * radio buttons are done in a way where clicking them when they're already toggled makes them execute the code again anyways
 *
 */

static Mouse m;
static size_t protocolBytes;
static int LEDBrightness;
static uint8_t responseRate; // 1/2/4/8 - 1000/500/250/125
static uint8_t modesEnabled; // 0-4
static uint8_t modeSelected; // 0-4
static int DPIMode1; // 1 - 50
static int DPIMode2; // 1 - 50
static int DPIMode3; // 1 - 50
static int DPIMode4; // 1 - 50
static int DPIMode5; // 1 - 50
static int currentDPI; // 1-50 // Can be different on other mice, no clue how to detect it yet
static int calibrationPreset; // 0 = calibration, 1 = preset enabled //8 = preset; c = calibration
static int calibration; // 1-16
static int keyResponse; // 1-16
static bool setupPhase; // Var used to not have UI elements trigger on init


MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow) {
		ui->setupUi(this);
		loadTextFile();
		initMouseNew();
	}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::on_findButton_clicked()
{
    QString searchString = ui->lineEdit->text();
    ui->textEdit->find(searchString, QTextDocument::FindWholeWords);
    Mouse m;
// testing
    vector<uint8_t> vectresponse = m.getMagicSensBytes(1,1,1,1,1,1);
	ostringstream stream;
    stream << hex << unsigned(vectresponse[0]) << " "<< unsigned(vectresponse[1]) << " "<< unsigned(vectresponse[2]) << " "<< unsigned(vectresponse[3]) << endl << dec;
    std::string str =  stream.str();
    const char* chr = str.c_str();
    ui->label->setText(chr);
}

void MainWindow::loadTextFile() {
    QFile inputFile(":/test.txt");
    inputFile.open(QIODevice::ReadOnly);

    QTextStream in(&inputFile);
    QString line = in.readAll();
    inputFile.close();

    ui->textEdit->setPlainText(line);
    QTextCursor cursor = ui->textEdit->textCursor();
    cursor.movePosition(QTextCursor::Start, QTextCursor::MoveAnchor, 1);
}

void MainWindow::on_listWidget_doubleClicked(const QModelIndex &index) {
	setupPhase = true;
	int address= ui->listWidget->currentItem()->toolTip().toInt();

	if(! m.selectDevice(address)){
		cout << "Err" << endl; // #TODO - this crashes if two apps try to access the same device. Crashes further in but should be checked for in here.
	}
	cout << "DETECTING MOUSE PROTOCOL (Takes long because it's shoddy)" << endl;

	// #TODO - redo this to check for interface .2 maximum packet size!!
	if ( static_cast<int>(m.getLEDBrightness(72)) == 255) {
		if (static_cast<int>(m.getLEDBrightness(64)) != 255) {
			protocolBytes = 64;
			cout<<"Detected device as one that uses 64 bytes in the protocol."<< endl;
		}
		else {
				cerr<<"UNSUPPORTED DEVICE"<<endl;
				exit(1);
			}
		// We got an error trying to use 72 bytes, and as there are only 2 known protocols, the mouse must therefore use 64 bytes.
	}
	else {
		protocolBytes = 72;
		cout<<"Detected device as one that uses 72 bytes in the protocol."<< endl;
	}
//	m.print_endpoint()
	cout<<"Current backlight level: "<<static_cast<int>(m.getLEDBrightness(protocolBytes))<< endl;
	ui->labelMouseID->setText("Mouse ID: "+QString::fromStdString(m.getMouseID(protocolBytes)));
	LEDBrightness = m.getLEDBrightness(protocolBytes);
	switch(LEDBrightness){
	 case 0: ui->radioButtonLEDBrightness0->setChecked(true);break;
	 case 1: ui->radioButtonLEDBrightness1->setChecked(true);break;
	 case 2: ui->radioButtonLEDBrightness2->setChecked(true);break;
	 case 3: ui->radioButtonLEDBrightness3->setChecked(true);break;
	}
	//
	calibrationPreset = 1; // #TODO - remove hack after getting info from magic bytes is implemented
	calibration = 1;       // #TODO - remove hack after getting info from magic bytes is implemented
	modesEnabled = 0x04;   // #TODO - remove hack after getting info from magic bytes is implemented
	modeSelected = 0x04;   // #TODO - remove hack after getting info from magic bytes is implemented
	responseRate = 0x01;   // #TODO - remove hack after getting info from magic bytes is implemented
	DPIMode1 = 10;         // #TODO - remove hack after getting info from magic bytes is implemented
	DPIMode2 = 20;         // #TODO - remove hack after getting info from magic bytes is implemented
	DPIMode3 = 30;         // #TODO - remove hack after getting info from magic bytes is implemented
	DPIMode4 = 40;         // #TODO - remove hack after getting info from magic bytes is implemented
	DPIMode5 = 50;         // #TODO - remove hack after getting info from magic bytes is implemented
	currentDPI = 49;       // #TODO - remove hack after getting info from magic bytes is implemented
	ui->spinBox->setMinimum(1);
	ui->spinBox->setMaximum(5);

	ui->comboBoxDPISens->clear();
	for (int i =1; i<51;i++) {
		ui->comboBoxDPISens->addItem(QString::fromStdString(to_string(i) + "00Hz"));
	}

	// Can't be detecting calibration through 0713, it should be done through magic bytes instead
	/*
	uint8_t a = m.getSensorCalibration(protocolBytes);
	uint8_t left  = m.leftUint8t(a);
	switch(left){
		case 0x08: ui->radioButtonCalibrationPreset->setChecked(true); break;
		case 0x0c: ui->radioButtonCalibrationCalibration->setChecked(true);break;
	}
	uint8_t right = m.rightUint8t(a);
	*/
	setupPhase = false;
}
/*
void MainWindow::initMouse(){
	vector<uint8_t> vectresponse = m.getMagicSensBytes(1,1,1,1,1,1);
	cout << hex << unsigned(vectresponse[0]) << " "<< unsigned(vectresponse[1]) << " "<< unsigned(vectresponse[2]) << " "<< unsigned(vectresponse[3]) << endl << dec;
	int address;
	m.init();
	do {
		const auto devices = m.getDevices();
		if(devices.size() == 1) {
			libusb_device* device = libusb_get_device(devices.begin()->second);
			libusb_device_descriptor desc;
			libusb_get_device_descriptor(device, &desc);
			cout << "Only one device found, selecting it: " << m.getDeviceNameById(desc.idProduct) <<" ["<< hex << desc.idProduct<<"]"<< endl;
			m.selectDevice(desc.idProduct);
			break;
		} else {
			do{
				m.listDevices();
				cout << "Enter device address: ";
				cin >> address;
				if (cin.fail()) {
					cerr<<"Error, bad input"<<endl;
					cin.clear();
					cin.ignore(512,'\n');
					continue;
				}
				cout << "Selected addresss: " <<address<<endl;
				break;
			}while(true);
		}
	} while(!m.selectDevice(address));

	// Protocol test - very shoddy detection
	cout << "DETECTING MOUSE PROTOCOL (Takes long because it's shoddy)" << endl;

	// #TODO - redo this to check for interface .2 maximum packet size!!
	if ( static_cast<int>(m.getLEDBrightness(72)) == 255) {
		if (static_cast<int>(m.getLEDBrightness(64)) != 255) {
            protocolBytes = 64;
            cout<<"Detected device as one that uses 64 bytes in the protocol."<< endl;
        }
        else {
                cerr<<"UNSUPPORTED DEVICE"<<endl;
                exit(1);
            }
        // We got an error trying to use 72 bytes, and as there are only 2 known protocols, the mouse must therefore use 64 bytes.
    }
    else {
        protocolBytes = 72;
        cout<<"Detected device as one that uses 72 bytes in the protocol."<< endl;
    }
//	m.print_endpoint()
	cout<<"Current backlight level: "<<static_cast<int>(m.getLEDBrightness(protocolBytes))<< endl;
    ui->labelMouseID->setText("Mouse ID: "+QString::fromStdString(m.getMouseID(protocolBytes)));
	LEDBrightness = m.getLEDBrightness(protocolBytes);
	switch(LEDBrightness){
	 case 0: ui->radioButtonLEDBrightness0->setChecked(true);break;
	 case 1: ui->radioButtonLEDBrightness1->setChecked(true);break;
	 case 2: ui->radioButtonLEDBrightness2->setChecked(true);break;
	 case 3: ui->radioButtonLEDBrightness3->setChecked(true);break;
	}
	//
	calibrationPreset = 1; // #TODO - remove hack after getting info from magic bytes is implemented
	calibration = 1;       // #TODO - remove hack after getting info from magic bytes is implemented
	modesEnabled = 4;      // #TODO - remove hack after getting info from magic bytes is implemented
	modeSelected = 4;      // #TODO - remove hack after getting info from magic bytes is implemented
	currentDPI = 49;       // #TODO - remove hack after getting info from magic bytes is implemented
}*/

void MainWindow::on_radioButtonRR125_clicked(bool checked)
{
// 1/2/4/8 - 1000/500/250/125
	if (checked) {
		responseRate = 0x08;
		m.SetSensitivity(protocolBytes, modesEnabled, modeSelected, currentDPI, responseRate);
	}
}

void MainWindow::on_radioButtonRR250_clicked(bool checked)
{
	if (checked) {
		responseRate = 0x04;
		m.SetSensitivity(protocolBytes, modesEnabled, modeSelected, currentDPI, responseRate);
	}
}

void MainWindow::on_radioButtonRR500_clicked(bool checked)
{
	if (checked) {
		responseRate = 0x02;
		m.SetSensitivity(protocolBytes, modesEnabled, modeSelected, currentDPI, responseRate);
	}
}

void MainWindow::on_radioButtonRR1000_clicked(bool checked)
{
	if (checked) {
		responseRate = 0x01;
		m.SetSensitivity(protocolBytes, modesEnabled, modeSelected, currentDPI, responseRate);
	}
}

void MainWindow::on_radioButtonLEDBrightness0_clicked(bool checked)
{
	if (checked) {
		LEDBrightness = m.setLEDBrightness(protocolBytes, 0);
	}
}

void MainWindow::on_radioButtonLEDBrightness1_clicked(bool checked)
{
	if (checked) {
		LEDBrightness = m.setLEDBrightness(protocolBytes, 1);
	}
}

void MainWindow::on_radioButtonLEDBrightness2_clicked(bool checked)
{
	if (checked) {
		LEDBrightness = m.setLEDBrightness(protocolBytes, 2);
	}
}

void MainWindow::on_radioButtonLEDBrightness3_clicked(bool checked)
{
	if (checked) {
		LEDBrightness = m.setLEDBrightness(protocolBytes, 3);
	}
}
/*
void MainWindow::on_radioButton_2_clicked(bool checked)
{
	QMessageBox msgBox;
//    msgBox.setText(ui->listWidget->currentItem()->text());
	uint8_t a = m.getSensorCalibration(protocolBytes);
	uint8_t left  = m.leftUint8t(a);
	uint8_t right = m.rightUint8t(a);

	//    uint8_t first = a & 0xf0;
//    uint8_t second = a & 0x0f;
	string fuckshit = "a1";

//    uint8_t watafak = scanf("%2.2x", &fuckshit);
	ostringstream stream;
	std::string str =  stream.str();
	const char* chr = str.c_str();
	ui->label->setText(chr);
	std::string aaargh = "ad";
	stream << hex << static_cast<int>(left) << " and right: " << static_cast<int>(right);



//    string shit = sprintf("%X", m.getSensorCalibration(protocolBytes));
	msgBox.setText("First: " + QString::fromStdString(stream.str()) + " Second: ");
	msgBox.exec();

}
*/

void MainWindow::on_radioButtonCalibrationPreset_clicked(bool checked)
{
	calibrationPreset=1;
	if (checked) {
		m.setSensorCalibration(protocolBytes,calibrationPreset,calibration);
	}
}

void MainWindow::on_radioButtonCalibrationCalibration_clicked(bool checked)
{
	calibrationPreset=0;
	if (checked) {
		m.setSensorCalibration(protocolBytes,calibrationPreset,calibration);
	}
}


void MainWindow::on_comboBoxCalibration_currentIndexChanged(int index)
{
	calibration = index+1;
	m.setSensorCalibration(protocolBytes,calibrationPreset,calibration);
}

void MainWindow::on_pushButtonRefreshDevices_clicked()
{
	initMouseNew();
}

void MainWindow::initMouseNew(){
//	vector<uint8_t> vectresponse = m.getMagicSensBytes(1,1,1,1,1,1);
//	cout << hex << unsigned(vectresponse[0]) << " "<< unsigned(vectresponse[1]) << " "<< unsigned(vectresponse[2]) << " "<< unsigned(vectresponse[3]) << endl << dec;
//	int address;
	ui->listWidget->clear();
	m.init();

	vector<vector<string>> deviceReport = m.listDevices();
	for(auto vektorek : deviceReport){
		ui->listWidget->addItem(QString::fromStdString(vektorek[1] + " [" + m.string_to_hex(vektorek[2]) + "] FW(bcdDevice): " + m.string_to_hex(vektorek[3])));
		// Set tooltip for last added item
		ui->listWidget->item(ui->listWidget->count()-1)->setToolTip(QString::fromStdString(vektorek[0]));
	}
/*	const auto devices = m.getDevices();
		if(devices.size() == 1) {
			libusb_device* device = libusb_get_device(devices.begin()->second);
			libusb_device_descriptor desc;
			libusb_get_device_descriptor(device, &desc);
			cout << "Only one device found, selecting it: " << m.getDeviceNameById(desc.idProduct) <<" ["<< hex << desc.idProduct<<"]"<< endl;
			m.selectDevice(desc.idProduct);
		}

	// Protocol test - very shoddy detection
	cout << "DETECTING MOUSE PROTOCOL (Takes long because it's shoddy)" << endl;

	// #TODO - redo this to check for interface .2 maximum packet size!!
	if ( static_cast<int>(m.getLEDBrightness(72)) == 255) {
		if (static_cast<int>(m.getLEDBrightness(64)) != 255) {
			protocolBytes = 64;
			cout<<"Detected device as one that uses 64 bytes in the protocol."<< endl;
		}
		else {
				cerr<<"UNSUPPORTED DEVICE"<<endl;
				exit(1);
			}
		// We got an error trying to use 72 bytes, and as there are only 2 known protocols, the mouse must therefore use 64 bytes.
	}
	else {
		protocolBytes = 72;
		cout<<"Detected device as one that uses 72 bytes in the protocol."<< endl;
	}
//	m.print_endpoint()
	cout<<"Current backlight level: "<<static_cast<int>(m.getLEDBrightness(protocolBytes))<< endl;
	ui->labelMouseID->setText("Mouse ID: "+QString::fromStdString(m.getMouseID(protocolBytes)));
	LEDBrightness = m.getLEDBrightness(protocolBytes);
	switch(LEDBrightness){
	 case 0: ui->radioButtonLEDBrightness0->setChecked(true);break;
	 case 1: ui->radioButtonLEDBrightness1->setChecked(true);break;
	 case 2: ui->radioButtonLEDBrightness2->setChecked(true);break;
	 case 3: ui->radioButtonLEDBrightness3->setChecked(true);break;
	}
	//
	calibrationPreset = 1; // #TODO - remove hack after getting info from magic bytes is implemented
	calibration = 1;  // #TODO - remove hack after getting info from magic bytes is implemented

	// Can't be detecting calibration through 0713, it should be done through magic bytes instead

	uint8_t a = m.getSensorCalibration(protocolBytes);
	uint8_t left  = m.leftUint8t(a);
	switch(left){
		case 0x08: ui->radioButtonCalibrationPreset->setChecked(true); break;
		case 0x0c: ui->radioButtonCalibrationCalibration->setChecked(true);break;
	}
	uint8_t right = m.rightUint8t(a);
	*/
	// #TODO set initial response rate state based on the value from the 8 magic bytes
}

void MainWindow::on_pushButtonRestartFW_clicked()
{
	m.restartFirmware(protocolBytes);
}

void MainWindow::on_pushButtonGetRNG_clicked()
{
	vector<uint8_t> response = m.getRNG(protocolBytes);
	std::stringstream stream;
	stream << hex << static_cast<int>(response[0]) << static_cast<int>(response[1]);

	ui->pushButtonGetRNG->setText(QString::fromStdString(stream.str()));
}

void MainWindow::on_pushButtonDumpMemory_clicked()
{
	m.dumpMemory(protocolBytes);
}

void MainWindow::on_spinBox_valueChanged(int arg1)
{
	switch(arg1){
	case 1 :
		modesEnabled = 0;
		ui->pushButtonDPIMode2->hide();
		ui->pushButtonDPIMode3->hide();
		ui->pushButtonDPIMode4->hide();
		ui->pushButtonDPIMode5->hide();
		break;
	case 2 :
		modesEnabled = 1;
		ui->pushButtonDPIMode2->show();
		ui->pushButtonDPIMode3->hide();
		ui->pushButtonDPIMode4->hide();
		ui->pushButtonDPIMode5->hide();
		break;
	case 3 :
		modesEnabled = 2;
		ui->pushButtonDPIMode2->show();
		ui->pushButtonDPIMode3->show();
		ui->pushButtonDPIMode4->hide();
		ui->pushButtonDPIMode5->hide();
		break;
	case 4 :
		modesEnabled = 3;
		ui->pushButtonDPIMode2->show();
		ui->pushButtonDPIMode3->show();
		ui->pushButtonDPIMode4->show();
		ui->pushButtonDPIMode5->hide();
		break;
	case 5 :
		modesEnabled = 4;
		ui->pushButtonDPIMode2->show();
		ui->pushButtonDPIMode3->show();
		ui->pushButtonDPIMode4->show();
		ui->pushButtonDPIMode5->show();
		break;
	}
	m.SetSensitivity(protocolBytes, modesEnabled, modeSelected, currentDPI, responseRate);
}

void MainWindow::on_pushButtonDPIMode1_clicked()
{
	currentDPI = DPIMode1;
	modeSelected = 0x00;
	ui->pushButtonDPIMode1->setStyleSheet("font-weight: bold");
	ui->pushButtonDPIMode2->setStyleSheet("font-weight: normal");
	ui->pushButtonDPIMode3->setStyleSheet("font-weight: normal");
	ui->pushButtonDPIMode4->setStyleSheet("font-weight: normal");
	ui->pushButtonDPIMode5->setStyleSheet("font-weight: normal");
	ui->comboBoxDPISens->setCurrentIndex(DPIMode1-1);
	m.SetSensitivity(protocolBytes, modesEnabled, modeSelected, currentDPI, responseRate);
}

void MainWindow::on_pushButtonDPIMode2_clicked()
{
	currentDPI = DPIMode2;
	modeSelected = 0x01;
	ui->pushButtonDPIMode1->setStyleSheet("font-weight: normal");
	ui->pushButtonDPIMode2->setStyleSheet("font-weight: bold");
	ui->pushButtonDPIMode3->setStyleSheet("font-weight: normal");
	ui->pushButtonDPIMode4->setStyleSheet("font-weight: normal");
	ui->pushButtonDPIMode5->setStyleSheet("font-weight: normal");
	ui->comboBoxDPISens->setCurrentIndex(DPIMode2-1);
	m.SetSensitivity(protocolBytes, modesEnabled, modeSelected, currentDPI, responseRate);
}

void MainWindow::on_pushButtonDPIMode3_clicked()
{
	currentDPI = DPIMode3;
	modeSelected = 0x02;
	ui->pushButtonDPIMode1->setStyleSheet("font-weight: normal");
	ui->pushButtonDPIMode2->setStyleSheet("font-weight: normal");
	ui->pushButtonDPIMode3->setStyleSheet("font-weight: bold");
	ui->pushButtonDPIMode4->setStyleSheet("font-weight: normal");
	ui->pushButtonDPIMode5->setStyleSheet("font-weight: normal");
	ui->comboBoxDPISens->setCurrentIndex(DPIMode3-1);
	m.SetSensitivity(protocolBytes, modesEnabled, modeSelected, currentDPI, responseRate);
}

void MainWindow::on_pushButtonDPIMode4_clicked()
{
	currentDPI = DPIMode4;
	modeSelected = 0x03;
	ui->pushButtonDPIMode1->setStyleSheet("font-weight: normal");
	ui->pushButtonDPIMode2->setStyleSheet("font-weight: normal");
	ui->pushButtonDPIMode3->setStyleSheet("font-weight: normal");
	ui->pushButtonDPIMode4->setStyleSheet("font-weight: bold");
	ui->pushButtonDPIMode5->setStyleSheet("font-weight: normal");
	ui->comboBoxDPISens->setCurrentIndex(DPIMode4-1);
	m.SetSensitivity(protocolBytes, modesEnabled, modeSelected, currentDPI, responseRate);
}

void MainWindow::on_pushButtonDPIMode5_clicked()
{
	currentDPI = DPIMode5;
	modeSelected = 0x04;
	ui->pushButtonDPIMode1->setStyleSheet("font-weight: normal");
	ui->pushButtonDPIMode2->setStyleSheet("font-weight: normal");
	ui->pushButtonDPIMode3->setStyleSheet("font-weight: normal");
	ui->pushButtonDPIMode4->setStyleSheet("font-weight: normal");
	ui->pushButtonDPIMode5->setStyleSheet("font-weight: bold");
	ui->comboBoxDPISens->setCurrentIndex(DPIMode5-1);
	m.SetSensitivity(protocolBytes, modesEnabled, modeSelected, currentDPI, responseRate);
}

void MainWindow::on_comboBoxDPISens_currentIndexChanged(int index)
{
	if (setupPhase) return;
	switch (modeSelected){
	case 0x00:
		DPIMode1 = index+1;
		currentDPI = index+1;
		break;
	case 0x01:
		DPIMode2 = index+1;
		currentDPI = index+1;
		break;
	case 0x02:
		DPIMode3 = index+1;
		currentDPI = index+1;
		break;
	case 0x03:
		DPIMode4 = index+1;
		currentDPI = index+1;
		break;
	case 0x04:
		DPIMode5 = index+1;
		currentDPI = index+1;
		break;
	}
	m.SetSensitivity(protocolBytes, modesEnabled, modeSelected, currentDPI, responseRate);
}

void MainWindow::on_pushButtonExecuteCodes_clicked() {
	QString str = ui->textEditTestCodes->toPlainText();
	QStringList lines = str.split( "\n", QString::SkipEmptyParts );
	QString placeholderText = "";
	foreach( QString line, lines ) {
		vector<uint8_t> response = m.testCode(protocolBytes, line.toStdString());
		placeholderText += QString::fromStdString( m.AddSeparators( m.uint8_vector_to_hex_string(response) ) + "\n" );
	}
	ui->textEditTestCodes->setPlaceholderText(placeholderText);
	ui->textEditTestCodes->setText("");
}
